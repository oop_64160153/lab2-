public class JavaDataTypes09 {
    public static void main(String[] args) {
        boolean isJavaFun = true;
        boolean isFishTasty = false;
        System.out.println(isJavaFun); // true
        System.out.println(isFishTasty); // false
    }
}
