public class JavaTypeCasting01 {
    public static void main(String[] args) {
        int myInt = 9;
        double myDouble = myInt; // int to double
        System.out.println(myInt); // Outputs 9
        System.out.println(myDouble); // Outputs 9.0
    }
}
